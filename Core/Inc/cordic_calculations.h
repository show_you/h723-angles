/*
 * cordic_calculations.h
 *
 *  Created on: Feb 23, 2024
 *      Author: klpkv
 */

#ifndef INC_CORDIC_CALCULATIONS_H_
#define INC_CORDIC_CALCULATIONS_H_


/* Reference values in Q1.31 format!!!  */
#define MODULUS	(int32_t)0x7FFFFFFF	/* 1 */

typedef enum {
	COS_CALC = 1,
	SIN_CALC = 2,
	TAN_CALC = 3,
	ATAN_CALC = 4,
	ATAN2_CALC = 5,
	NO_CALC = 0,
} CORDIC_State;


void get_cordic_cos(float angle, float *cos, float *sin);
void get_cordic_atan(int32_t input, int32_t *atan);
void get_cordic_atan2(int32_t first, int32_t second, int32_t *atan2);


#endif /* INC_CORDIC_CALCULATIONS_H_ */
