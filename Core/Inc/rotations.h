#ifndef __ROTATIONS_H
#define __ROTATIONS_H


float fast_atan_f(float x);
float fast_atan2_f(float y, float x);
double fast_atan_d(double x);
double fast_atan2_d(double y, double x);

float get_roll(int16_t ax, int16_t ay, int16_t az);
float get_pitch(float roll, int16_t ax, int16_t ay, int16_t az);
float get_yaw(float roll, float pitch, int16_t mx, int16_t my, int16_t mz);

void get_rpy_angles(int16_t ax, int16_t ay, int16_t az, int16_t mx, int16_t my, int16_t mz,
		float *roll, float *pitch, float *yaw);

float get_alpha(float roll);
float get_beta(float roll, float pitch);
float get_gamma(float yaw);

void get_abg_angles(float roll, float pitch, float yaw, float *alpha, float *beta, float *gamma);

void get_rotation_matrix_around_vector(float x, float y, float z, float cosTheta, float sinTheta, float *matrix);
void multiply_3x3_matrix(float *m1, float *m2, float *mResult);
void get_global_rotation_matrix(float alpha, float beta, float gamma, float *matrix);
void rotate_coordinates(float x0, float y0, float z0, float *rotationMatrix, float *x, float *y, float *z);

float degrees(float angle);
float radians(float angle);

#endif /* __ROTATIONS_H */
