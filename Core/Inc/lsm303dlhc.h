/*
 * lsm303dlhc.h
 *
 *  Created on: Feb 27, 2024
 *      Author: klpkv
 */

#ifndef INC_LSM303DLHC_H_
#define INC_LSM303DLHC_H_

#include "stdint.h"


#define ACC_I2C_ADDRESS		0x32
#define MAG_I2C_ADDRESS		0x3C

#define LSM303DLHC_NORMAL_MODE		((uint8_t) 0x00)
#define LSM303DLHC_LOWPOWER_MODE	((uint8_t) 0x08)

#define LSM303DLHC_ODR_1_HZ			((uint8_t) 0x10)	/* Output data rate = 1 Hz */
#define LSM303DLHC_ODR_10_HZ		((uint8_t) 0x20)	/* Output data rate = 10 Hz */
#define LSM303DLHC_ODR_25_HZ		((uint8_t) 0x30)	/* Output data rate = 25 Hz */
#define LSM303DLHC_ODR_50_HZ		((uint8_t) 0x40)	/* Output data rate = 50 Hz */
#define LSM303DLHC_ODR_100_HZ		((uint8_t) 0x50)	/* Output data rate = 100 Hz */
#define LSM303DLHC_ODR_200_HZ		((uint8_t) 0x60)	/* Output data rate = 200 Hz */
#define LSM303DLHC_ODR_400_HZ		((uint8_t) 0x70)	/* Output data rate = 400 Hz */
#define LSM303DLHC_ODR_1620_HZ_LP	((uint8_t) 0x80)	/* Output data rate = 1620 Hz only in low power mode */
#define LSM303DLHC_ODR_1344_HZ		((uint8_t) 0x90)	/* Output data rate = 1344 Hz in normal mode and 5376 Hz in low power mode */

#define LSM303DLHC_X_ENABLE			((uint8_t) 0x01)	/*  */
#define LSM303DLHC_Y_ENABLE			((uint8_t) 0x02)	/*  */
#define LSM303DLHC_Z_ENABLE			((uint8_t) 0x04)	/*  */
#define LSM303DLHC_AXES_ENABLE		((uint8_t) 0x07)	/*  */
#define LSM303DLHC_AXES_DISABLE		((uint8_t) 0x00)	/*  */

#define LSM303DLHC_HR_ENABLE		((uint8_t) 0x08)
#define LSM303DLHC_HR_DISABLE		((uint8_t) 0x00)

#define LSM303DLHC_FULLSCALE_2G		((uint8_t) 0x00)	/* +-2g */
#define LSM303DLHC_FULLSCALE_4G		((uint8_t) 0x10)	/* +-4g */
#define LSM303DLHC_FULLSCALE_8G		((uint8_t) 0x20)	/* +-8g */
#define LSM303DLHC_FULLSCALE_16G	((uint8_t) 0x30)	/* +-16g */

#define LSM303DLHC_BlockUpdate_Continues	((uint8_t) 0x00)	/* continues update */
#define LSM303DLHC_BlockUpdate_Single		((uint8_t) 0x80)	/* single update - output registers not updated until MSB and LSB reading */

#define LSM303DLHC_BLE_LSB			((uint8_t) 0x00)	/* little endian: data LSB @ lower address */
#define LSM303DLHC_BLE_MSB			((uint8_t) 0x40)	/* big endian: data MSB @ lower address */

#define LSM303DLHC_HPM_NORMAL_MODE_RES	((uint8_t) 0x00)
#define LSM303DLHC_HPM_REF_SIGNAL		((uint8_t) 0x40)
#define LSM303DLHC_HPM_NORMAL_MODE		((uint8_t) 0x80)
#define LSM303DLHC_HPM_AUTORESET_INT	((uint8_t) 0xC0)

#define LSM303DLHC_HPFCF_8			((uint8_t) 0x00)
#define LSM303DLHC_HPFCF_16			((uint8_t) 0x10)
#define LSM303DLHC_HPFCF_32			((uint8_t) 0x20)
#define LSM303DLHC_HPFCF_64			((uint8_t) 0x30)

#define LSM303DLHC_HPF_AOI1_DISABLE	((uint8_t) 0x00)
#define LSM303DLHC_HPF_AOI1_ENABLE	((uint8_t) 0x01)

#define LSM303DLHC_HPF_AOI2_DISABLE	((uint8_t) 0x00)
#define LSM303DLHC_HPF_AOI2_ENABLE	((uint8_t) 0x02)

#define LSM303DLHC_CTRL_REG1_A		0x20	/* control register 1 acceleration */
#define LSM303DLHC_CTRL_REG2_A		0x21	/* control register 2 acceleration */
#define LSM303DLHC_CTRL_REG3_A		0x22	/* control register 3 acceleration */
#define LSM303DLHC_CTRL_REG4_A		0x23	/* control register 4 acceleration */
#define LSM303DLHC_CTRL_REG5_A		0x24	/* control register 5 acceleration */
#define LSM303DLHC_CTRL_REG6_A		0x25	/* control register 6 acceleration */
#define LSM303DLHC_CRA_REG_M		0x00	/* control register A magnetic field */
#define LSM303DLHC_CRB_REG_M		0x01	/* control register B magnetic field */
#define LSM303DLHC_MR_REG_M			0x02	/* control register MR magnetic field */

#define LSM303DLHC_ACC_SENSITIVITY_2G	((uint8_t) 1)	/* accelerometer sensitivity with 2 g full scale [mg/LSB] */
#define LSM303DLHC_ACC_SENSITIVITY_4G	((uint8_t) 2)	/* accelerometer sensitivity with 4 g full scale [mg/LSB] */
#define LSM303DLHC_ACC_SENSITIVITY_8G	((uint8_t) 4)	/* accelerometer sensitivity with 8 g full scale [mg/LSB] */
#define LSM303DLHC_ACC_SENSITIVITY_16G	((uint8_t) 12)	/* accelerometer sensitivity with 12 g full scale [mg/LSB] */

#define LSM303DLHC_OUT_X_L_A		0x28	/* output register X acceleration */
#define LSM303DLHC_OUT_X_H_A		0x29	/* output register X acceleration */
#define LSM303DLHC_OUT_Y_L_A		0x2A	/* output register y acceleration */
#define LSM303DLHC_OUT_Y_H_A		0x2B	/* output register y acceleration */
#define LSM303DLHC_OUT_Z_L_A		0x2C	/* output register z acceleration */
#define LSM303DLHC_OUT_Z_H_A		0x2D	/* output register z acceleration */

#define LSM303DLHC_OUT_X_H_M		0x03	/* output register X magnetic field */
#define LSM303DLHC_OUT_X_L_M		0x04	/* output register X magnetic field */
#define LSM303DLHC_OUT_Z_H_M		0x05	/* output register Z magnetic field */
#define LSM303DLHC_OUT_Z_L_M		0x06	/* output register Z magnetic field */
#define LSM303DLHC_OUT_Y_H_M		0x07	/* output register Y magnetic field */
#define LSM303DLHC_OUT_Y_L_M		0x08	/* output register Y magnetic field */

#define LSM303DLHC_TEMPSENSOR_ENABLE	((uint8_t) 0x80)	/* temp sensor enable */
#define LSM303DLHC_TEMPSENSOR_DISABLE	((uint8_t) 0x00)	/* temp sensor disable */

#define LSM303DLHC_ODR_0_75_HZ		((uint8_t) 0x00)	/* Output data rate = 0.75 Hz */
#define LSM303DLHC_ODR_1_5_HZ		((uint8_t) 0x04)	/* Output data rate = 1.5 Hz */
#define LSM303DLHC_ODR_3_0_HZ		((uint8_t) 0x08)	/* Output data rate = 3 Hz */
#define LSM303DLHC_ODR_7_5_HZ		((uint8_t) 0x0C)	/* Output data rate = 7.5 Hz */
#define LSM303DLHC_ODR_15_HZ		((uint8_t) 0x10)	/* Output data rate = 15 Hz */
#define LSM303DLHC_ODR_30_HZ		((uint8_t) 0x14)	/* Output data rate = 30 Hz */
#define LSM303DLHC_ODR_75_HZ		((uint8_t) 0x18)	/* Output data rate = 75 Hz */
#define LSM303DLHC_ODR_220_HZ		((uint8_t) 0x1C)	/* Output data rate = 220 Hz */

#define LSM303DLHC_FS_1_3_GA		((uint8_t) 0x20)	/* full scale = +- 1.3 Gauss */
#define LSM303DLHC_FS_1_9_GA		((uint8_t) 0x40)	/* full scale = +- 1.9 Gauss */
#define LSM303DLHC_FS_2_5_GA		((uint8_t) 0x60)	/* full scale = +- 2.5 Gauss */
#define LSM303DLHC_FS_4_0_GA		((uint8_t) 0x80)	/* full scale = +- 4.0 Gauss */
#define LSM303DLHC_FS_4_7_GA		((uint8_t) 0xA0)	/* full scale = +- 4.7 Gauss */
#define LSM303DLHC_FS_5_6_GA		((uint8_t) 0xC0)	/* full scale = +- 5.6 Gauss */
#define LSM303DLHC_FS_8_1_GA		((uint8_t) 0xE0)	/* full scale = +- 8.1 Gauss */

#define LSM303DLHC_CONTINUOUS_CONVERSION	((uint8_t) 0x00)	/* continuous-conversion mode */
#define LSM303DLHC_SINGLE_CONVERSION		((uint8_t) 0x01)	/* single-conversion mode */
#define LSM303DLHC_SLEEP					((uint8_t) 0x02)	/* sleep mode */


void lsm_init(void);
uint8_t lsm_read_ID(void);
uint8_t lsm_read(uint16_t deviceAddress, uint8_t registerAddress);
void lsm_write(uint16_t deviceAddress, uint8_t registerAddress, uint8_t value);

void accel_init(uint16_t initStruct);
void mag_init(uint32_t initStruct);
void accel_filter_config(uint8_t filterStruct);
void accel_get_xyz(int16_t *pData);
void mag_get_xyz(int16_t *pData);

uint8_t I2Cx_read_data(uint16_t addr, uint8_t reg);
void I2Cx_write_data(uint16_t addr, uint8_t reg, uint8_t value);



#endif /* INC_LSM303DLHC_H_ */
