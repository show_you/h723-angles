/*
 * cordic_calculations.c
 *
 *  Created on: Feb 23, 2024
 *      Author: klpkv
 */
#include "main.h"
#include "cordic_calculations.h"


CORDIC_State cordicState = NO_CALC;


void get_cordic_cos(float angle, float *cos, float *sin)
{
	if (cordicState != COS_CALC) {
		LL_CORDIC_Config(CORDIC, LL_CORDIC_FUNCTION_COSINE,
								 LL_CORDIC_PRECISION_6CYCLES,
								 LL_CORDIC_SCALE_0,
								 LL_CORDIC_NBWRITE_2,
								 LL_CORDIC_NBREAD_2,
								 LL_CORDIC_INSIZE_32BITS,
								 LL_CORDIC_OUTSIZE_32BITS);
		cordicState = COS_CALC;
	}

	int32_t cordicAngle = (int32_t) (angle * (1UL << 31) / M_PI_F);

	LL_CORDIC_WriteData(CORDIC, cordicAngle);
	LL_CORDIC_WriteData(CORDIC, MODULUS);
	int32_t cosOutput = (int32_t) LL_CORDIC_ReadData(CORDIC);
	*cos = (float) cosOutput / (float) 0x80000000;
	int32_t sinOutput = (int32_t) LL_CORDIC_ReadData(CORDIC);
	*sin = (float) sinOutput / (float) 0x80000000;
}


void get_cordic_atan(int32_t input, int32_t *atan)
{
	if (cordicState != ATAN_CALC) {
		LL_CORDIC_Config(CORDIC, LL_CORDIC_FUNCTION_ARCTANGENT,
								 LL_CORDIC_PRECISION_6CYCLES,
								 LL_CORDIC_SCALE_0,
								 LL_CORDIC_NBWRITE_1,
								 LL_CORDIC_NBREAD_1,
								 LL_CORDIC_INSIZE_32BITS,
								 LL_CORDIC_OUTSIZE_32BITS);
		cordicState = ATAN_CALC;
	}

	LL_CORDIC_WriteData(CORDIC, input);
	*atan = (int32_t) LL_CORDIC_ReadData(CORDIC);
}


void get_cordic_atan2(int32_t first, int32_t second, int32_t *atan2)
{
	if (cordicState != ATAN2_CALC) {
		LL_CORDIC_Config(CORDIC, LL_CORDIC_FUNCTION_PHASE,
								 LL_CORDIC_PRECISION_6CYCLES,
								 LL_CORDIC_SCALE_0,
								 LL_CORDIC_NBWRITE_2,
								 LL_CORDIC_NBREAD_1,
								 LL_CORDIC_INSIZE_32BITS,
								 LL_CORDIC_OUTSIZE_32BITS);
		cordicState = ATAN2_CALC;
	}

	LL_CORDIC_WriteData(CORDIC, first);
	LL_CORDIC_WriteData(CORDIC, second);
	*atan2 = (int32_t) LL_CORDIC_ReadData(CORDIC);
}

