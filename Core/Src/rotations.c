#include "main.h"
#include "cordic_calculations.h"


const double M_PI_D   = 3.141592653589793;
const double M_PI_2_D = 1.5707963267948966;
const double M_PI_4_D = 0.7853981633974483;


float cosRoll;
float sinRoll;


double fast_atan_d(double x)
{
	return M_PI_4_D * x - x * (fabs(x) - 1) * (0.2447 + 0.0663 * fabs(x));
}


double fast_atan2_d(double y, double x)
{
	if (x >= 0) { // -pi/2 .. pi/2
		if (y >= 0) { // 0 .. pi/2
			if (y < x) { // 0 .. pi/4
				return fast_atan_d(y / x);
			} else { // pi/4 .. pi/2
				return M_PI_2_D - fast_atan_d(x / y);
			}
		} else {
			if (-y < x) { // -pi/4 .. 0
				return fast_atan_d(y / x);
			} else { // -pi/2 .. -pi/4
				return -M_PI_2_D - fast_atan_d(x / y);
			}
		}
	} else { // -pi..-pi/2, pi/2..pi
		if (y >= 0) { // pi/2 .. pi
			if (y < -x) { // pi*3/4 .. pi
				return fast_atan_d(y / x) + M_PI_D;
			} else { // pi/2 .. pi*3/4
				return M_PI_2_D - fast_atan_d(x / y);
			}
		} else { // -pi .. -pi/2
			if (-y < -x) { // -pi .. -pi*3/4
				return fast_atan_d(y / x) - M_PI_D;
			} else { // -pi*3/4 .. -pi/2
				return -M_PI_2_D - fast_atan_d(x / y);
			}
		}
	}
}


float fast_atan_f(float x)
{
	return M_PI_4_F * x - x * (fabs(x) - 1) * (0.2447 + 0.0663 * fabs(x));
}


float fast_atan2_f(float y, float x)
{
    //http://pubs.opengroup.org/onlinepubs/009695399/functions/atan2.html
    //Volkan SALMA

    const float ONEQTR_PI = M_PI_4_F;
	const float THRQTR_PI = 3.0 * M_PI_4_F;
	float r, angle;
	float abs_y = fabs(y) + 1e-10f;      // kludge to prevent 0/0 condition
	if ( x < 0.0f )
	{
		r = (x + abs_y) / (abs_y - x);
		angle = THRQTR_PI;
	}
	else
	{
		r = (x - abs_y) / (x + abs_y);
		angle = ONEQTR_PI;
	}
	angle += (0.1963f * r * r - 0.9817f) * r;
	if ( y < 0.0f )
		return( -angle );     // negate if in quad III or IV
	else
		return( angle );


}

/*
 * roll angle [-180...180]
 * angle of device rotation around its x axe
 * is positive when projection of device's y axe on the Z axe is positive
 * is 0 when device's y axe is parallel to XY plane and device's z axe looks up
 * if pitch angle = ±90 deg, then ay and az are near-zero and roll is unstable; if
 * stability is desired, then az can be substituted by az+ax*alpha, with
 * alpha = 0.01-0.05.
 * result value is in radians
 * */
float get_roll(int16_t ax, int16_t ay, int16_t az)
{
	return fast_atan2_d(ay, 1.0 * az + ax * 0.01);
}


/* pitch angle [-90...90]
 * angle between device's x axe and XY plane
 * is positive when projection of device's x axe on the Z axe is negative
 * if pitch = ±90 deg, then ay and az are near-zero and az2 is also near-zero;
 * division by zero should not be performed, pitch = -90 deg if ax>0, and
 * pitch = +90 deg if ax<0; ax cannot be zero
 * result value is in radians
 * */
float get_pitch(float roll, int16_t ax, int16_t ay, int16_t az)
{
	if (-2 < ay && ay < 2 && -2 < az && az < 2) {
		if (ax >= 0) {
			return -M_PI_2_F;
		} else {
			return M_PI_2_F;
		}
	} else {
		/* Normalization of roll is needed!!! Roll must be [-1;1] */
		get_cordic_cos(roll, &cosRoll, &sinRoll);
		float az2 = ay * sinRoll + az * cosRoll;
		return fast_atan_d((-ax * 1.0) / az2);
	}
}


/* yaw angle [-180...180]
 * is 0 when projection of device's x axe on XY plane is directed towards the magnetic north pole
 * positive angle direction is left
 * If pitch = ±90 deg and if roll is unstable, yaw will also be unstable; if roll is
 * made stable as mentioned above, then yaw will also be stable.
 * Regardless of stability, the sum Roll+Yaw will always be stable
 * result value is in radians
 * */
float get_yaw(float pitch, int16_t mx, int16_t my, int16_t mz)
{
    float mz2 = my * sinRoll + mz * cosRoll;
    float sinPitch, cosPitch;
    /* Normalization of pitch is needed!!! Pitch must be [-1;1] */
    get_cordic_cos(pitch, &cosPitch, &sinPitch);
    float mx3 = mx * cosPitch + mz2 * sinPitch;
    return fast_atan2_d(mz * sinRoll - my * cosRoll, mx3);
}


/* Get base state angles: roll, pitch, yaw */
void get_rpy_angles(int16_t ax, int16_t ay, int16_t az, int16_t mx, int16_t my, int16_t mz,
		float *roll, float *pitch, float *yaw)
{
	*roll = get_roll(ax, ay, az);
	*pitch = get_pitch(*roll, ax, ay, az);
	*yaw = get_yaw(*pitch, mx, my, mz);
}


/* Get modified roll angle to conform rotation matrix */
float get_alpha(float roll)
{
	if (roll < 0) {
		return M_PI_F * 2 + roll;
	} else {
		return roll;
	}
}


/* Get modified pitch angle to conform rotation matrix */
float get_beta(float roll, float pitch)
{
	if (roll <= M_PI_2_F && roll >= -M_PI_2_F) {
		return pitch;
	} else {
		return -pitch;
	}
}


/* Get modified yaw angle to conform rotation matrix */
float get_gamma(float yaw)
{
	if (yaw < 0) {
		return M_PI_F * 2 + yaw;
	} else {
		return yaw;
	}
}


/* Get modified positional angles to conform rotation matrix */
void get_abg_angles(float roll, float pitch, float yaw, float *alpha, float *beta, float *gamma)
{
	*alpha = get_alpha(roll);
	*beta = get_beta(roll, pitch);
	*gamma = get_gamma(yaw);
}


/* Get rotation matrix around single vector with xyz coordinates */
void get_rotation_matrix_around_vector(float x, float y, float z, float cosTheta, float sinTheta, float *matrix)
{
	float cosTheta_x = (1.0 - cosTheta) * x;
	float cosTheta_y = (1.0 - cosTheta) * y;
	float cosTheta_z = (1.0 - cosTheta) * z;
	float cosTheta_x_y = cosTheta_x * y;
	float cosTheta_x_z = cosTheta_x * z;
	float cosTheta_z_y = cosTheta_z * y;
	float sinTheta_x = sinTheta * x;
	float sinTheta_y = sinTheta * y;
	float sinTheta_z = sinTheta * z;

	matrix[0] = cosTheta + cosTheta_x * x;
	matrix[1] = cosTheta_x_y - sinTheta_z;
	matrix[2] = cosTheta_x_z + sinTheta_y;

	matrix[3] = cosTheta_x_y + sinTheta_z;
	matrix[4] = cosTheta + cosTheta_y * y;
	matrix[5] = cosTheta_z_y - sinTheta_x;

	matrix[6] = cosTheta_x_z - sinTheta_y;
	matrix[7] = cosTheta_z_y + sinTheta_x;
	matrix[8] = cosTheta + cosTheta_z * z;
}


/* Multiply 3x3 matrix */
void multiply_3x3_matrix(float *m1, float *m2, float *mResult)
{
	mResult[0] = m1[0] * m2[0] + m1[1] * m2[3] + m1[2] * m2[6];
	mResult[1] = m1[0] * m2[1] + m1[1] * m2[4] + m1[2] * m2[7];
	mResult[2] = m1[0] * m2[2] + m1[1] * m2[5] + m1[2] * m2[8];

	mResult[3] = m1[3] * m2[0] + m1[4] * m2[3] + m1[5] * m2[6];
	mResult[4] = m1[3] * m2[1] + m1[4] * m2[4] + m1[5] * m2[7];
	mResult[5] = m1[3] * m2[2] + m1[4] * m2[5] + m1[5] * m2[8];

	mResult[6] = m1[6] * m2[0] + m1[7] * m2[3] + m1[8] * m2[6];
	mResult[7] = m1[6] * m2[1] + m1[7] * m2[4] + m1[8] * m2[7];
	mResult[8] = m1[6] * m2[2] + m1[7] * m2[5] + m1[8] * m2[8];
}


/* Get global rotation matrix, consist of rotations around x, y and z  */
void get_global_rotation_matrix(float alpha, float beta, float gamma, float *matrix)
{
	float sinAlpha, cosAlpha, sinBeta, cosBeta, sinGamma, cosGamma;

	get_cordic_cos(alpha, &cosAlpha, &sinAlpha);
	get_cordic_cos(beta, &cosBeta, &sinBeta);
	get_cordic_cos(gamma, &cosGamma, &sinGamma);

	float mAlpha[9], mBeta[9], mGamma[9], mMulty1[9];

	get_rotation_matrix_around_vector(0.0, 0.0, 1.0, cosGamma, sinGamma, mGamma);
	get_rotation_matrix_around_vector(-sinGamma, cosGamma, 0.0, cosBeta, sinBeta, mBeta);
	get_rotation_matrix_around_vector(cosGamma * cosBeta, sinGamma * cosBeta, sinBeta, cosAlpha, sinAlpha, mAlpha);

	multiply_3x3_matrix(mAlpha, mBeta, mMulty1);
	multiply_3x3_matrix(mMulty1, mGamma, matrix);
}


/* Get new coordinates after rotation */
void rotate_coordinates(float x0, float y0, float z0, float *rotationMatrix, float *x, float *y, float *z)
{
	*x = x0 * rotationMatrix[0] + y0 * rotationMatrix[1] + z0 * rotationMatrix[2];
	*y = x0 * rotationMatrix[3] + y0 * rotationMatrix[4] + z0 * rotationMatrix[5];
	*z = x0 * rotationMatrix[6] + y0 * rotationMatrix[7] + z0 * rotationMatrix[8];
}


/* Get angle in degrees */
float degrees(float angle)
{
	return angle / M_PI_F * 180.0;
}


/* Get angle in radians*/
float radians(float angle)
{
	return angle / 180.0 * M_PI_F;
}
















































