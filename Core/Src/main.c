/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"

#include "rotations.h"
#include "lsm303dlhc.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

I2C_HandleTypeDef hi2c1;

SAI_HandleTypeDef hsai_BlockA1;
SAI_HandleTypeDef hsai_BlockB1;
SAI_HandleTypeDef hsai_BlockA4;
SAI_HandleTypeDef hsai_BlockB4;
DMA_HandleTypeDef hdma_sai1_a;
DMA_HandleTypeDef hdma_sai1_b;
DMA_HandleTypeDef hdma_sai4_a;
DMA_HandleTypeDef hdma_sai4_b;

UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

volatile int32_t sampleBufferSAI1A[MIC_SAMPLES_PER_PACKET];
volatile int32_t sampleBufferSAI1B[MIC_SAMPLES_PER_PACKET];


#if defined ( __CC_ARM )
volatile int32_t sampleBufferSAI4A[MIC_SAMPLES_PER_PACKET] __attribute__((at(0x38000000)));
volatile int32_t sampleBufferSAI4B[MIC_SAMPLES_PER_PACKET] __attribute__((at(0x38000000)));
#elif defined ( __ICCARM )
#pragma location=0x38000000
volatile int32_t sampleBufferSAI4A[MIC_SAMPLES_PER_PACKET];
volatile int32_t sampleBufferSAI4B[MIC_SAMPLES_PER_PACKET];
#elif defined ( __GNUC__ )
volatile int32_t sampleBufferSAI4A[MIC_SAMPLES_PER_PACKET] __attribute__((section(".RAM_D3")));
volatile int32_t sampleBufferSAI4B[MIC_SAMPLES_PER_PACKET] __attribute__((section(".RAM_D3")));
#endif


int32_t sendBufferSAI1A_1ch[SEND_BUFFER_SIZE] = {0xfffff1a1};
int32_t sendBufferSAI1A_2ch[SEND_BUFFER_SIZE] = {0xfffff1a2};
//sendBufferSAI1A_1ch[0] = 0xfffff1a1;
//sendBufferSAI1A_2ch[0] = 0xfffff1a2;
uint16_t sendBufferSAI1ASize = 1;

int32_t sendBufferSAI1B_1ch[SEND_BUFFER_SIZE] = {0xfffff1b1};
int32_t sendBufferSAI1B_2ch[SEND_BUFFER_SIZE] = {0xfffff1b2};
//sendBufferSAI1B_1ch[0] = 0xfffff1b1;
//sendBufferSAI1B_2ch[0] = 0xfffff1b2;
uint16_t sendBufferSAI1BSize = 1;

int32_t sendBufferSAI4A_1ch[SEND_BUFFER_SIZE] = {0xfffff4a1};
int32_t sendBufferSAI4A_2ch[SEND_BUFFER_SIZE] = {0xfffff4a2};
//sendBufferSAI4A_1ch[0] = 0xfffff4a1;
//sendBufferSAI4A_2ch[0] = 0xfffff4a2;
uint16_t sendBufferSAI4ASize = 1;

int32_t sendBufferSAI4B_1ch[SEND_BUFFER_SIZE] = {0xfffff4b1};
int32_t sendBufferSAI4B_2ch[SEND_BUFFER_SIZE] = {0xfffff4b2};
//sendBufferSAI4B_1ch[0] = 0xfffff4b1;
//sendBufferSAI4B_2ch[0] = 0xfffff4b2;
uint16_t sendBufferSAI4BSize = 1;

DMA_SAI_state sai1a_state = STATE_NONE;
DMA_SAI_state sai1b_state = STATE_NONE;
DMA_SAI_state sai4a_state = STATE_NONE;
DMA_SAI_state sai4b_state = STATE_NONE;

SAI_order sai_order = ORDER_NONE;

Send_State sending_state = NO_SEND;

uint8_t readyToSend = 0;
uint8_t startRecording = 0;

extern USBD_HandleTypeDef hUsbDeviceHS;


/* test rotation coordinates purpose*/
int16_t accelerometer[7][3] = {{-28, -14, 976}, {72, 1032, 263}, {63, 692, 687}, {84, 381, 889}, {93, 265, 963}, {-385, -60, 894}, {-505, 20, 799}};
int16_t magneticSensor[7][3] = {{91, -172, -352}, {45, -308, -61}, {33, -283, -158}, {27, -215, -246}, {24, -187, -267}, {124, -109, -265}, {146, -138, -238}};
float coordinateX = 0.57735;
float coordinateY = 0.57735;
float coordinateZ = 0.57735;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_BDMA_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_SAI4_Init(void);
static void MX_SAI1_Init(void);
static void MX_CORDIC_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */
void sendData();
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_BDMA_Init();
  MX_USART3_UART_Init();
  MX_SAI4_Init();
  MX_SAI1_Init();
  MX_USB_DEVICE_Init();
  MX_CORDIC_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  lsm_init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  HAL_SAI_Receive_DMA(&hsai_BlockA1, (uint8_t*) sampleBufferSAI1A, MIC_SAMPLES_PER_PACKET);
  HAL_SAI_Receive_DMA(&hsai_BlockB1, (uint8_t*) sampleBufferSAI1B, MIC_SAMPLES_PER_PACKET);
  HAL_SAI_Receive_DMA(&hsai_BlockA4, (uint8_t*) sampleBufferSAI4A, MIC_SAMPLES_PER_PACKET);
  HAL_SAI_Receive_DMA(&hsai_BlockB4, (uint8_t*) sampleBufferSAI4B, MIC_SAMPLES_PER_PACKET);


  /* count operation time code */
  CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; // Разрешаем TRACE
  DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk; // Разрешаем счетчик тактов
  volatile uint32_t cycles_count;

  while (1)
  {
	  if (startRecording == 1) {
		  HAL_Delay(1000);
		  sending_state = SENDING;
		  startRecording = 0;
	  }
	  if (readyToSend == 1) {
		  sendData();

		  if (HAL_GPIO_ReadPin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin) == GPIO_PIN_SET) {
//			  HAL_Delay(100);
			  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);
		  }

		  readyToSend = 0;
	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  if (HAL_GPIO_ReadPin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin) == GPIO_PIN_RESET) {
		  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_SET);
		  float roll, pitch, yaw, alpha, beta, gamma;
		  float rollD, pitchD, yawD, alphaD, betaD, gammaD;
		  float matrix[9];
		  float x, y, z;

		  int16_t accelData[3] = {0};
		  int16_t magData[3] = {0};

		  accel_get_xyz(accelData);
		  mag_get_xyz(magData);

		  DWT->CYCCNT = 0; // Обнуляем счетчик

		  get_rpy_angles(accelData[0], accelData[1], accelData[2],
				  	  	 magData[0], magData[1], magData[2],
						 &roll, &pitch, &yaw);
		  get_abg_angles(roll, pitch, yaw, &alpha, &beta, &gamma);
		  get_global_rotation_matrix(roll, beta, yaw, matrix);
		  rotate_coordinates(coordinateX, coordinateY, coordinateZ, matrix, &x, &y, &z);

		  cycles_count = DWT->CYCCNT;

		  rollD = degrees(roll);
		  pitchD = degrees(pitch);
		  yawD = degrees(yaw);
		  alphaD = degrees(alpha);
		  betaD = degrees(beta);
		  gammaD = degrees(gamma);

		  if (rollD > pitchD && yawD > alphaD && betaD == gammaD && cycles_count == 0) {
			  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);
			  HAL_Delay(300);
			  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_SET);
		  }

		  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);
	  } else {
		  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);
	  }

	  /* code to get rotation calculation results on test data */
//	  if (HAL_GPIO_ReadPin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin) == GPIO_PIN_RESET) {
//		  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_SET);
//		  for (int8_t i = 0; i < 7; i++) {
//			  float roll, pitch, yaw, alpha, beta, gamma;
//			  float rollD, pitchD, yawD, alphaD, betaD, gammaD;
//			  float matrix[9];
//			  float x, y, z;
//
//			  get_rpy_angles(accelerometer[i][0], accelerometer[i][1], accelerometer[i][2],
//							 magneticSensor[i][0], magneticSensor[i][1], magneticSensor[i][2],
//							 &roll, &pitch, &yaw);
//			  get_abg_angles(roll, pitch, yaw, &alpha, &beta, &gamma);
//			  get_global_rotation_matrix(roll, beta, yaw, matrix);
//			  rotate_coordinates(coordinateX, coordinateY, coordinateZ, matrix, &x, &y, &z);
//
//			  rollD = degrees(roll);
//			  pitchD = degrees(pitch);
//			  yawD = degrees(yaw);
//			  alphaD = degrees(alpha);
//			  betaD = degrees(beta);
//			  gammaD = degrees(gamma);
//
//			  if (rollD > pitchD && yawD > alphaD && betaD == gammaD) {
//				  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);
//				  HAL_Delay(300);
//				  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_SET);
//			  }
//		  }
//
//		  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);
//	  } else {
//		  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);
//	  }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_LDO_SUPPLY);

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE0);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 62;
  RCC_OscInitStruct.PLL.PLLP = 1;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 4096;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CORDIC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CORDIC_Init(void)
{

  /* USER CODE BEGIN CORDIC_Init 0 */

  /* USER CODE END CORDIC_Init 0 */

  /* Peripheral clock enable */
  __HAL_RCC_CORDIC_CLK_ENABLE();

  /* USER CODE BEGIN CORDIC_Init 1 */

  /* USER CODE END CORDIC_Init 1 */

  /* nothing else to be configured */

  /* USER CODE BEGIN CORDIC_Init 2 */

  /* USER CODE END CORDIC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x20A0A3F6;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SAI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SAI1_Init(void)
{

  /* USER CODE BEGIN SAI1_Init 0 */

  /* USER CODE END SAI1_Init 0 */

  /* USER CODE BEGIN SAI1_Init 1 */

  /* USER CODE END SAI1_Init 1 */
  hsai_BlockA1.Instance = SAI1_Block_A;
  hsai_BlockA1.Init.AudioMode = SAI_MODEMASTER_RX;
  hsai_BlockA1.Init.Synchro = SAI_ASYNCHRONOUS;
  hsai_BlockA1.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
  hsai_BlockA1.Init.NoDivider = SAI_MASTERDIVIDER_ENABLE;
  hsai_BlockA1.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
  hsai_BlockA1.Init.AudioFrequency = SAI_AUDIO_FREQUENCY_48K;
  hsai_BlockA1.Init.SynchroExt = SAI_SYNCEXT_OUTBLOCKA_ENABLE;
  hsai_BlockA1.Init.MonoStereoMode = SAI_STEREOMODE;
  hsai_BlockA1.Init.CompandingMode = SAI_NOCOMPANDING;
  if (HAL_SAI_InitProtocol(&hsai_BlockA1, SAI_I2S_STANDARD, SAI_PROTOCOL_DATASIZE_24BIT, 2) != HAL_OK)
  {
    Error_Handler();
  }
  hsai_BlockB1.Instance = SAI1_Block_B;
  hsai_BlockB1.Init.AudioMode = SAI_MODESLAVE_RX;
  hsai_BlockB1.Init.Synchro = SAI_SYNCHRONOUS;
  hsai_BlockB1.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
  hsai_BlockB1.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
  hsai_BlockB1.Init.SynchroExt = SAI_SYNCEXT_OUTBLOCKA_ENABLE;
  hsai_BlockB1.Init.MonoStereoMode = SAI_STEREOMODE;
  hsai_BlockB1.Init.CompandingMode = SAI_NOCOMPANDING;
  hsai_BlockB1.Init.TriState = SAI_OUTPUT_NOTRELEASED;
  if (HAL_SAI_InitProtocol(&hsai_BlockB1, SAI_I2S_STANDARD, SAI_PROTOCOL_DATASIZE_24BIT, 2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SAI1_Init 2 */

  /* USER CODE END SAI1_Init 2 */

}

/**
  * @brief SAI4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SAI4_Init(void)
{

  /* USER CODE BEGIN SAI4_Init 0 */

  /* USER CODE END SAI4_Init 0 */

  /* USER CODE BEGIN SAI4_Init 1 */

  /* USER CODE END SAI4_Init 1 */
  hsai_BlockA4.Instance = SAI4_Block_A;
  hsai_BlockA4.Init.AudioMode = SAI_MODESLAVE_RX;
  hsai_BlockA4.Init.Synchro = SAI_SYNCHRONOUS_EXT_SAI1;
  hsai_BlockA4.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
  hsai_BlockA4.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
  hsai_BlockA4.Init.MonoStereoMode = SAI_STEREOMODE;
  hsai_BlockA4.Init.CompandingMode = SAI_NOCOMPANDING;
  hsai_BlockA4.Init.TriState = SAI_OUTPUT_NOTRELEASED;
  if (HAL_SAI_InitProtocol(&hsai_BlockA4, SAI_I2S_STANDARD, SAI_PROTOCOL_DATASIZE_24BIT, 2) != HAL_OK)
  {
    Error_Handler();
  }
  hsai_BlockB4.Instance = SAI4_Block_B;
  hsai_BlockB4.Init.AudioMode = SAI_MODESLAVE_RX;
  hsai_BlockB4.Init.Synchro = SAI_SYNCHRONOUS_EXT_SAI1;
  hsai_BlockB4.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
  hsai_BlockB4.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
  hsai_BlockB4.Init.MonoStereoMode = SAI_STEREOMODE;
  hsai_BlockB4.Init.CompandingMode = SAI_NOCOMPANDING;
  hsai_BlockB4.Init.TriState = SAI_OUTPUT_NOTRELEASED;
  if (HAL_SAI_InitProtocol(&hsai_BlockB4, SAI_I2S_STANDARD, SAI_PROTOCOL_DATASIZE_24BIT, 2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SAI4_Init 2 */

  /* USER CODE END SAI4_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart3, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart3, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_BDMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_BDMA_CLK_ENABLE();

  /* DMA interrupt init */
  /* BDMA_Channel0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(BDMA_Channel0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(BDMA_Channel0_IRQn);
  /* BDMA_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(BDMA_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(BDMA_Channel1_IRQn);

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream0_IRQn);
  /* DMA1_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_GREEN_Pin|LED_RED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_FS_PWR_EN_GPIO_Port, USB_FS_PWR_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_MDC_Pin RMII_RXD0_Pin RMII_RXD1_Pin */
  GPIO_InitStruct.Pin = RMII_MDC_Pin|RMII_RXD0_Pin|RMII_RXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_REF_CLK_Pin RMII_MDIO_Pin RMII_CRS_DV_Pin */
  GPIO_InitStruct.Pin = RMII_REF_CLK_Pin|RMII_MDIO_Pin|RMII_CRS_DV_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_GREEN_Pin LED_RED_Pin */
  GPIO_InitStruct.Pin = LED_GREEN_Pin|LED_RED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : RMII_TXD1_Pin */
  GPIO_InitStruct.Pin = RMII_TXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(RMII_TXD1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_FS_PWR_EN_Pin */
  GPIO_InitStruct.Pin = USB_FS_PWR_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_FS_PWR_EN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_FS_OVCR_Pin */
  GPIO_InitStruct.Pin = USB_FS_OVCR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_FS_OVCR_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_TX_EN_Pin RMII_TXD0_Pin */
  GPIO_InitStruct.Pin = RMII_TX_EN_Pin|RMII_TXD0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : LED_YELLOW_Pin */
  GPIO_InitStruct.Pin = LED_YELLOW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_YELLOW_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */


void sendData() {
	/* SAI1A 1ch */
	CDC_Transmit_HS((uint8_t*) sendBufferSAI1A_1ch, DATA_COUNT_TO_SEND * 4);
	HAL_Delay(1000);
	/* SAI1A 2ch */
	CDC_Transmit_HS((uint8_t*) sendBufferSAI1A_2ch, DATA_COUNT_TO_SEND * 4);
	HAL_Delay(1000);

	/* SAI1B 1ch */
	CDC_Transmit_HS((uint8_t*) sendBufferSAI1B_1ch, DATA_COUNT_TO_SEND * 4);
	HAL_Delay(1000);
	/* SAI1B 2ch */
	CDC_Transmit_HS((uint8_t*) sendBufferSAI1B_2ch, DATA_COUNT_TO_SEND * 4);
	HAL_Delay(1000);

	/* SAI4A 1ch */
	CDC_Transmit_HS((uint8_t*) sendBufferSAI4A_1ch, DATA_COUNT_TO_SEND * 4);
	HAL_Delay(1000);
	/* SAI4A 2ch */
	CDC_Transmit_HS((uint8_t*) sendBufferSAI4A_2ch, DATA_COUNT_TO_SEND * 4);
	HAL_Delay(1000);

	/* SAI4B 1ch */
	CDC_Transmit_HS((uint8_t*) sendBufferSAI4B_1ch, DATA_COUNT_TO_SEND * 4);
	HAL_Delay(1000);
	/* SAI4B 2ch */
	CDC_Transmit_HS((uint8_t*) sendBufferSAI4B_2ch, DATA_COUNT_TO_SEND * 4);
	HAL_Delay(1000);
}


void prepareData(DMA_SAI_state* state, volatile int32_t* data_src, int32_t* data_dst_1ch, int32_t* data_dst_2ch, uint16_t* currentSize) {
	uint16_t start_idx = 0;
	if (*state == DMA_SAI_STATE_FULL) {
		start_idx = MIC_SAMPLES_PER_PACKET / 2;
	}

	if (*currentSize < SEND_BUFFER_SIZE) {
		for (uint16_t i = 0; i < MIC_SAMPLES_PER_PACKET / 2; i = i + 2) {
			data_dst_1ch[*currentSize + i / 2] = data_src[start_idx + i];
			data_dst_2ch[*currentSize + i / 2] = data_src[start_idx + i + 1];
		}
		*currentSize += MIC_SAMPLES_PER_PACKET / 4;
	} else {
		sending_state = NO_SEND;
		HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, GPIO_PIN_SET);
		readyToSend = 1;
//		sendData();
	}
}


void HAL_SAI_RxHalfCpltCallback(SAI_HandleTypeDef *hsai) {
	if (sending_state == SENDING) {
		if (hsai->Instance == SAI1_Block_A) {
			sai1a_state = DMA_SAI_STATE_HALF;
			prepareData(&sai1a_state, sampleBufferSAI1A, sendBufferSAI1A_1ch, sendBufferSAI1A_2ch, &sendBufferSAI1ASize);
		} else if (hsai->Instance == SAI1_Block_B) {
			sai1b_state = DMA_SAI_STATE_HALF;
			prepareData(&sai1b_state, sampleBufferSAI1B, sendBufferSAI1B_1ch, sendBufferSAI1B_2ch, &sendBufferSAI1BSize);
		} else if (hsai->Instance == SAI4_Block_A) {
			sai4a_state = DMA_SAI_STATE_HALF;
			prepareData(&sai4a_state, sampleBufferSAI4A, sendBufferSAI4A_1ch, sendBufferSAI4A_2ch, &sendBufferSAI4ASize);
		} else if (hsai->Instance == SAI4_Block_B) {
			sai4b_state = DMA_SAI_STATE_HALF;
			prepareData(&sai4b_state, sampleBufferSAI4B, sendBufferSAI4B_1ch, sendBufferSAI4B_2ch, &sendBufferSAI4BSize);
		}
	}
}

void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hsai) {
	if (sending_state == SENDING) {
		if (hsai->Instance == SAI1_Block_A) {
			sai1a_state = DMA_SAI_STATE_FULL;
			prepareData(&sai1a_state, sampleBufferSAI1A, sendBufferSAI1A_1ch, sendBufferSAI1A_2ch, &sendBufferSAI1ASize);
		} else if (hsai->Instance == SAI1_Block_B) {
			sai1b_state = DMA_SAI_STATE_FULL;
			prepareData(&sai1b_state, sampleBufferSAI1B, sendBufferSAI1B_1ch, sendBufferSAI1B_2ch, &sendBufferSAI1BSize);
		} else if (hsai->Instance == SAI4_Block_A) {
			sai4a_state = DMA_SAI_STATE_FULL;
			prepareData(&sai4a_state, sampleBufferSAI4A, sendBufferSAI4A_1ch, sendBufferSAI4A_2ch, &sendBufferSAI4ASize);
		} else if (hsai->Instance == SAI4_Block_B) {
			sai4b_state = DMA_SAI_STATE_FULL;
			prepareData(&sai4b_state, sampleBufferSAI4B, sendBufferSAI4B_1ch, sendBufferSAI4B_2ch, &sendBufferSAI4BSize);
		}
	}
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if (GPIO_Pin == B1_Pin) {
		if (sending_state == NO_SEND) {
			sendBufferSAI1ASize = 1;
			sendBufferSAI1BSize = 1;
			sendBufferSAI4ASize = 1;
			sendBufferSAI4BSize = 1;

			startRecording = 1;
		}
	}
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
