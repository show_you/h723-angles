/*
 * lsm303dlhc.c
 *
 *  Created on: Feb 27, 2024
 *      Author: klpkv
 */
#include "main.h"
#include "lsm303dlhc.h"


extern I2C_HandleTypeDef hi2c1;


static void I2C_error(void)
{
	/* Led blink? */
	HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
}


void lsm_init(void)
{
	uint16_t ctrl = 0x0000;
	uint32_t ctrl32 = 0x00000000;

	HAL_Delay(1000);

	if (lsm_read_ID() != 0x33) {
		I2C_error();
	}

	ctrl |= (LSM303DLHC_NORMAL_MODE | LSM303DLHC_ODR_100_HZ | LSM303DLHC_AXES_ENABLE);
	ctrl |= ((LSM303DLHC_BlockUpdate_Continues | LSM303DLHC_BLE_LSB | LSM303DLHC_HR_ENABLE) << 8);
	accel_init(ctrl);

//	ctrl = (uint8_t) (LSM303DLHC_HPM_NORMAL_MODE | LSM303DLHC_HPFCF_16 | LSM303DLHC_HPF_AOI1_DISABLE | LSM303DLHC_HPF_AOI2_DISABLE);
//	accel_filter_config(ctrl);

	ctrl32 |= (LSM303DLHC_TEMPSENSOR_DISABLE | LSM303DLHC_ODR_220_HZ);
	ctrl32 |= (LSM303DLHC_FS_4_0_GA << 8);
	ctrl32 |= (LSM303DLHC_CONTINUOUS_CONVERSION << 16);
	mag_init(ctrl32);
}


void accel_init(uint16_t initStruct)
{
	uint8_t ctrl = 0x00;
	ctrl = (uint8_t) initStruct;
	lsm_write(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG1_A, ctrl);
	ctrl = (uint8_t) (initStruct >> 8);
	lsm_write(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG4_A, ctrl);
}


void mag_init(uint32_t initStruct)
{
	uint8_t ctrl = 0x00;
	ctrl = (uint8_t) initStruct;
	lsm_write(MAG_I2C_ADDRESS, LSM303DLHC_CRA_REG_M, ctrl);
	ctrl = (uint8_t) (initStruct >> 8);
	lsm_write(MAG_I2C_ADDRESS, LSM303DLHC_CRB_REG_M, ctrl);
	ctrl = (uint8_t) (initStruct >> 16);
	lsm_write(MAG_I2C_ADDRESS, LSM303DLHC_MR_REG_M, ctrl);
}


void accel_filter_config(uint8_t filterStruct)
{
	uint8_t tmpreg;

	/* read CTRL_REG2 register */
	tmpreg = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG2_A);

	tmpreg &= 0x0C;
	tmpreg |= filterStruct;

	/* write value to CTRL_REG2 register */
	lsm_write(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG2_A, tmpreg);
}


uint8_t lsm_read_ID(void)
{
	uint8_t ctrl = 0x00;
	ctrl = lsm_read(ACC_I2C_ADDRESS, 0x0F);
	return ctrl;
}


uint8_t lsm_read(uint16_t deviceAddress, uint8_t registerAddress)
{
	return I2Cx_read_data(deviceAddress, registerAddress);
}


void lsm_write(uint16_t deviceAddress, uint8_t registerAddress, uint8_t value)
{
	I2Cx_write_data(deviceAddress, registerAddress, value);
}


void accel_get_xyz(int16_t *pData)
{
	int16_t pnRawData[3];
	uint8_t ctrlx[2] = {0, 0};
	uint8_t buffer[6];
	uint8_t i = 0;
	uint8_t sensitivity = LSM303DLHC_ACC_SENSITIVITY_2G;

	/* read accelerometer control register */
	ctrlx[0] = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG4_A);
	ctrlx[1] = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG5_A);

	/* read output xyz registers */
	buffer[0] = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_OUT_X_L_A);
	buffer[1] = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_OUT_X_H_A);
	buffer[2] = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_OUT_Y_L_A);
	buffer[3] = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_OUT_Y_H_A);
	buffer[4] = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_OUT_Z_L_A);
	buffer[5] = lsm_read(ACC_I2C_ADDRESS, LSM303DLHC_OUT_Z_H_A);

	/* check control register 4 for data alignment */
	if (!(ctrlx[0] & LSM303DLHC_BLE_MSB)) {
		for (i = 0; i < 3; i++) {
			pnRawData[i] = ((int16_t) ((uint16_t) buffer[2*i+1] << 8) + buffer[2*i]);
		}
	} else {
		for (i = 0; i < 3; i++) {
			pnRawData[i] = ((int16_t) ((uint16_t) buffer[2*i] << 8) + buffer[2*i+1]);
		}
	}

	/* normal mode */
	/* switch sensitivity value */
	switch (ctrlx[0] & LSM303DLHC_FULLSCALE_16G) {
		case LSM303DLHC_FULLSCALE_2G:
			sensitivity = LSM303DLHC_ACC_SENSITIVITY_2G;
			break;
		case LSM303DLHC_FULLSCALE_4G:
			sensitivity = LSM303DLHC_ACC_SENSITIVITY_4G;
			break;
		case LSM303DLHC_FULLSCALE_8G:
			sensitivity = LSM303DLHC_ACC_SENSITIVITY_8G;
			break;
		case LSM303DLHC_FULLSCALE_16G:
			sensitivity = LSM303DLHC_ACC_SENSITIVITY_16G;
			break;
	}

	/* obtain mg values for xyz */
	/* 16 is very important!!! */
	for (i = 0; i < 3; i++) {
		pData[i] = (pnRawData[i] * sensitivity) / 16;
	}
}


void mag_get_xyz(int16_t *pData)
{
	uint8_t buffer[6];
	uint8_t i = 0;

	/* read output xyz registers */
	buffer[0] = lsm_read(MAG_I2C_ADDRESS, LSM303DLHC_OUT_X_H_M);
	buffer[1] = lsm_read(MAG_I2C_ADDRESS, LSM303DLHC_OUT_X_L_M);
	buffer[2] = lsm_read(MAG_I2C_ADDRESS, LSM303DLHC_OUT_Y_H_M);
	buffer[3] = lsm_read(MAG_I2C_ADDRESS, LSM303DLHC_OUT_Y_L_M);
	buffer[4] = lsm_read(MAG_I2C_ADDRESS, LSM303DLHC_OUT_Z_H_M);
	buffer[5] = lsm_read(MAG_I2C_ADDRESS, LSM303DLHC_OUT_Z_L_M);

	/* obtain values for xyz */
	for (i = 0; i < 3; i++) {
		if (pData[i] != -4096) {
			pData[i] = ((int16_t) ((uint16_t) buffer[2*i] << 8) + buffer[2*i+1]);
		}
	}
}


uint8_t I2Cx_read_data(uint16_t addr, uint8_t reg)
{
	HAL_StatusTypeDef status = HAL_OK;
	uint8_t value = 0;
	status = HAL_I2C_Mem_Read(&hi2c1, addr, reg, I2C_MEMADD_SIZE_8BIT, &value, 1, 0x10000);

	if (status != HAL_OK) {
		/* Error!!! */
		I2C_error();
	}

	return value;
}


void I2Cx_write_data(uint16_t addr, uint8_t reg, uint8_t value)
{
	HAL_StatusTypeDef status = HAL_OK;
	status = HAL_I2C_Mem_Write(&hi2c1, addr, (uint16_t) reg, I2C_MEMADD_SIZE_8BIT, &value, 1, 0x10000);

	if (status != HAL_OK) {
		I2C_error();
	}
}
